import os
from threading import Thread
import time
import yaml

from pyfiglet import Figlet

from .team import (
    Roster,
    LineUp,
    Player,
    POSITION_BLOCKER,
    POSITION_PIVOT,
    POSITION_JAMMER,
)
from .official import Flamingos, Timer
from .jam import Jam, JamObjectFactory
from .speaker import Speaker, BasePrinter, printer_factory, PRINTER_TERMINAL


class ColorList:
    """List of color"""

    def __init__(self, l_color: list):
        self.l_color = l_color

    def rand(self):
        """Return a random color"""

        return self.l_color[0]


class Game(Thread):
    game_id: int = 0

    def __init__(self, app_conf: dict = {}):
        """Init

        Args:
            app_conf (dict, optional): additional config. Defaults to {}.
        """
        super().__init__()

        # Load default user config
        with open("conf/user.yaml") as file:
            user_conf: dict[str, object] = yaml.load(file, Loader=yaml.FullLoader)
        user_conf.update(app_conf)  # Priority to app conf

        # Team config
        d_team = dict()
        l_team = user_conf.get("color_team", ["blue", "yellow"])
        for i in range(user_conf.get("number_of_teams", 2)):
            team_color = l_team[i]
            d_team[team_color] = os.path.join(
                user_conf.get("path_teams"), team_color + ".csv"
            )

        # SO Practice
        d_practice: dict[str, bool] = {}
        d_practice["SK"] = user_conf.get("SK", False)
        d_practice["PBT"] = user_conf.get("PBT", False)
        d_practice["LT"] = user_conf.get("LT", False)

        # Game config
        d_game = dict()
        d_game["second_duration"] = user_conf.get("second_duration", 1.0)
        d_game["language"] = user_conf.get("language", "FR")
        d_game["speaker_name"] = user_conf.get("speaker_name", "Pipilate")
        d_game["time_period"] = user_conf.get("time_period", 3.0)
        d_game["app_mode"] = user_conf.get("app_mode")

        # Game attributes
        self.d_game: dict[str, object] = d_game
        self.d_team: dict[str, object] = d_team
        self.d_practice: dict[str, object] = d_practice
        self.jam: Jam = None
        self.printer: BasePrinter = None

        # Used to manage Thread
        self.game_on: bool = False
        self.game_alive: bool = False
        self.game_started = False

    @classmethod
    def get_game_id(cls) -> int:
        return cls.game_id

    def game_is_on(self) -> bool:
        return self.game_on

    def game_is_alive(self) -> bool:
        return self.game_alive

    def game_is_started(self) -> bool:
        return self.game_started

    def early_stop(self) -> bool:
        # No early stop if no game
        if not self.game_on:
            return False

        # Stop
        self.game_on = False  # stop the while in run method
        self.jam.stop()  # stop the jam in run method (set game_alive: False)
        self.join()

        return True

    def end_game(self) -> bool:
        # No join if the game is still running
        if self.game_alive:
            return False

        # Wait the end of the period time
        self.join()

        return True

    def run(self) -> bool:
        """Run Derby Game Thread"""

        if self.game_on:
            return False
        self.game_on = True
        self.game_alive = True
        self.game_started = True

        Game.game_id += 1

        # Build Rosters
        l_color = list()
        d_roster = dict()
        for color in self.d_team:
            l_color.append(color)
            roster = Roster(self.d_team[color], color)
            d_roster[color] = roster

        # Build LineUp
        lineup = LineUp(d_roster)
        choose_color = ColorList(l_color)
        second_duration = self.d_game["second_duration"]

        # Game objects
        # - Used to time the game
        timer = Timer()
        timer.reset_all()

        # - Print actions
        self.printer: BasePrinter = printer_factory(
            printer_type=self.d_game.get("app_mode", PRINTER_TERMINAL)
        )

        # - Comment actions
        self.speaker = Speaker(
            d_practice=self.d_practice,
            language=self.d_game["language"],
            speaker_name=self.d_game["speaker_name"],
            queue_printer=self.printer.get_queue(),
        )

        # - All NSO
        self.flamingos = Flamingos(d_roster)

        # - Used to build new jam
        jam_factory = JamObjectFactory(
            flamingos=self.flamingos,
            speaker=self.speaker,
            second_duration=second_duration,
        )
        self.jam = Jam(jam_factory=jam_factory)

        # Start simulation
        # - Print a beautiful screen
        f = Figlet(font="slant")
        str_prez = f.renderText("Derby Game Simulator")
        print(str_prez)

        # Start game
        self.printer.start()
        id_p = self.flamingos.new_period()
        self.speaker.speaker_sb("PERIOD " + str(id_p))
        time_period_seconds = self.d_game["time_period"] * 60
        while (time_period_seconds > 10) and (self.game_on):
            # Init Jam
            # - Choose the jammer that will obtain the lead
            lead_color = choose_color.rand()
            # - Formation lines
            d_position: dict[str, list[Player]] = {
                POSITION_BLOCKER: [],
                POSITION_JAMMER: [],
                POSITION_PIVOT: [],
            }
            # - Lineups select jam Player
            lead_jammer_name: str = None  # lead jammer is chosen here
            l_jamer_pivot: list[str] = []
            l_player: list[Player] = []
            # -- Determine jam players in a team
            all_players: list[Player] = []
            for team_color in self.d_team:
                l_line: list[Player] = lineup.new_line(team_color)
                all_players.extend(l_line)
                team_jammer_name: str = None
                team_pivot_name: str = None
                # --- Determine position for each player
                for player in l_line:
                    l_player.append(player)
                    position = player.get_position()
                    d_position[position].append(player)
                    if position == POSITION_JAMMER:
                        team_jammer_name = player.get_derby_name()
                        if lead_color == team_color:
                            lead_jammer_name = team_jammer_name
                    elif position == POSITION_PIVOT:
                        team_pivot_name = player.get_derby_name()
                # --- Record couple (jammer, pivot)
                l_jamer_pivot.append((team_jammer_name, team_pivot_name))

            # Fives seconds: announce and waiting
            self.speaker.speaker_jt("FIVE SECONDS")
            timer.five_seconds()
            all_players: list[Player] = [
                player for player in all_players if not player.is_in_box()
            ]
            self.speaker.speaker_lineup(lineup=all_players)
            time.sleep(5 * self.d_game["second_duration"])
            time_period_seconds -= 5

            # Jam
            id_jam = self.flamingos.new_jam(d_position)
            self.speaker.speaker_sb(f"JAM {id_jam}")
            self.speaker.speak_whistle_start_jam()
            time_jam = self.jam.start(
                l_player=l_player,
                lead_jammer_name=lead_jammer_name,
                l_jammer_pivot=l_jamer_pivot,
            )
            time_period_seconds -= time_jam
            self.speaker.speak_whistle_end_jam()

            # Inter Jam
            if time_period_seconds > 15:
                self.speaker.speaker_sb("INTERJAM")
                timer.add_seconds(25)

        # End game
        self._end_of_the_game()

        return True

    def _end_of_the_game(self) -> None:
        self.game_alive = False
        self.game_on = False
        self.speaker.speaker_sb("END OF THE GAME")
        self.speaker.print_log()
        self.flamingos.end_game()
        self.printer.stop()
        self.printer.join()

    def print_on_browser(self) -> str:
        if self.printer is not None:
            return self.printer.print_on_browser()
        return "No game"
