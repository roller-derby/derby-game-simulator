import yaml
import random
import copy


class PenaltyGenerator(object):
    """Penalty generator"""

    def __init__(self, language: str = "FR", position: str = "B", proba: float = 0.1):
        self.language = language
        self.position = position[0]  # Fist letter of the position
        self.proba_penalty = proba

        # Penalty config
        with open("conf/rules/penalty.yaml") as file:
            penalty_conf: dict[str, object] = yaml.load(file, Loader=yaml.FullLoader)
        self.duration = penalty_conf.get("penalty_duration", 30)
        self.l_penalty = penalty_conf.get("penalty", [])
        self.max_penalty = penalty_conf.get("nb_max_penalties", 2)
        self.max_blocker_box = penalty_conf.get("max_blocker_box", 2)

    def get_max_blocker_box(self):
        return self.max_blocker_box

    def generate(self) -> dict[str, object]:
        """Generate a list of penalties"""

        l_out = list()
        for i in range(1, self.max_penalty + 1):
            if random.random() < self.proba_penalty**i:
                # Select a penalty
                id_penalty = random.randint(0, len(self.l_penalty) - 1)
                d_penalty = copy.deepcopy(self.l_penalty[id_penalty])

                # Select a penalty description
                l_description = d_penalty["description_fr"]
                if self.language == "EN":
                    l_description = d_penalty["description_en"]

                id_description = random.randint(0, len(l_description) - 1)
                description = l_description[id_description]

                d_penalty["duration"] = self.duration
                d_penalty["description"] = description

                # Append the penalty
                l_out.append(d_penalty)

        return l_out
