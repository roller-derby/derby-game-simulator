import asyncio


def get_or_create_eventloop():
  """ get_event_loop raises error if not in MainThread, replacing asyncio.get_event_loop
  by this function solves the pb. Solution from
  https://techoverflow.net/2020/10/01/how-to-fix-python-asyncio-runtimeerror-there-is-no
  -current-event-loop-in-thread/
  """
  try:
    return asyncio.get_event_loop()
  except RuntimeError as ex:
    if "There is no current event loop in thread" in str(ex):
      loop = asyncio.new_event_loop()
      asyncio.set_event_loop(loop)
      return asyncio.get_event_loop()


def cancel_done_tasks():
  [task.cancel() for task in asyncio.Task.all_tasks() if task.done()]
