import asyncio

from abc import ABC, abstractmethod


KEY_SLEEPING_TIME = "sleeping_time"


class BaseJamObject(ABC):
    def __init__(self, object_name: str, sleeping_time: float = 1.0):
        self.object_name: str = object_name
        self.sleeping_time: float = sleeping_time  # Time to sleep between two tasks
        self.jam: bool = False

    def is_running(self) -> bool:
        return self.jam

    def get_id(self) -> str:
        return self.object_name

    def first_action(self):
        """
        First action in Jam
        """
        pass

    @abstractmethod
    def core_action(self) -> None:
        """
        Define jam actions
        """
        raise NotImplementedError

    def last_action(self):
        """
        Last jam action
        """
        pass

    async def class_task(self) -> None:
        self.first_action()
        await asyncio.sleep(self.sleeping_time)
        while self.jam:
            self.core_action()
            await asyncio.sleep(self.sleeping_time)
        self.last_action()

    def start(self) -> bool:
        self.jam = True
        try:
            asyncio.create_task(self.class_task())
        except Exception as e:
            print(f"Object '{self.object_name}' was not created because '{e}'")
            self.jam = False
            return False
        return True

    def stop(self) -> bool:
        if not self.jam:
            return False
        self.jam = False
        return True
