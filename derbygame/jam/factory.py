from derbygame.team import Player, POSITION_BLOCKER, POSITION_JAMMER, POSITION_PIVOT
from derbygame.speaker import Speaker
from derbygame.official import Flamingos

from .utils.base_jam_object import BaseJamObject
from .player.blocker import Blocker
from .player.pivot import Pivot
from .player.jammer import Jammer
from .jamtimer import JamTimer


class JamObjectFactory:
    def __init__(
        self,
        flamingos: Flamingos = Flamingos(),
        speaker: Speaker = Speaker(),
        second_duration: float = 0.1,
    ) -> None:
        # Object that not change between jams
        self.flamingos = flamingos
        self.speaker = speaker
        self.second_duration = second_duration

    def get_second_duration(self) -> float:
        return self.second_duration

    def jam_timer(self) -> JamTimer:
        return JamTimer(speaker=self.speaker, second_duration=self.second_duration)

    def player(self, player: Player = Player()) -> BaseJamObject:
        position = player.get_position()
        if position == POSITION_BLOCKER:
            return Blocker(
                player=player,
                speaker=self.speaker,
                flamingos=self.flamingos,
                second_duration=self.second_duration,
            )
        elif position == POSITION_PIVOT:
            return Pivot(
                player=player,
                speaker=self.speaker,
                flamingos=self.flamingos,
                second_duration=self.second_duration,
            )
        elif position == POSITION_JAMMER:
            return Jammer(
                player=player,
                speaker=self.speaker,
                flamingos=self.flamingos,
                second_duration=self.second_duration,
            )
