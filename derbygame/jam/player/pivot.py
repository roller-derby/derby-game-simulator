import random

from derbygame.team import Player, POSITION_JAMMER
from .blocker import Flamingos, Speaker, Blocker, PROBA_PENALTY

PROBA_NEW_TRIP = 0.4


class Pivot(Blocker):
    def __init__(
        self,
        player: Player = Player(),
        flamingos: Flamingos = Flamingos(),
        speaker: Speaker = Speaker(),
        second_duration: float = 1.0,
    ):
        Blocker.__init__(self, player, flamingos, speaker, second_duration)
        self.has_star_cover: bool = False
        self.min_trip_time = random.randint(2, 7)

    def get_number(self):
        return self.player.get_number()

    def star_pass(self):
        """Pivot obtains the star cover (legally)"""

        self.has_star_cover = True
        self.player.set_position(POSITION_JAMMER)

    def penalty_action(self) -> bool:
        is_a_penalty = super().penalty_action()
        if is_a_penalty and self.has_star_cover:  # Pivot is the Jammer
            serve_other = self.flamingos.check_jammer_in_box(
                self.player.get_color()
            )  # Libertate other Jammer, if other Jammer in box
            if serve_other > 0:
                time_to_server_reduced = min(
                    serve_other, self.player.get_time_to_serve()
                )
                self.player.set_time_to_serve(time_to_serve=time_to_server_reduced)
        return is_a_penalty

    def new_trip(self) -> int:
        """New trip"""

        # Player is not the Jammer
        if not self.has_star_cover:
            return 0

        num_blocker = self.flamingos.get_other_blocker_in_box(
            self.player.get_color()
        )  # Number of adv. blocker in box
        score = random.randint(num_blocker, 4)  # Random score

        # Flamingos
        num_trip = self.flamingos.new_trip(
            self.player.get_color(), score
        )  # Return the trip number

        # Speaker
        if num_trip == 1:  # initial pass
            self.speaker.speak_initial(self.player)
        else:  # speaker says the score pass
            self.speaker.speak_score(self.player, score)

        return num_trip

    def core_action(self) -> None:
        box_action = self.box_action()
        if not box_action:
            # No Jammer action if Jammer is outside the pack
            if self.has_star_cover:
                self.min_trip_time -= 1
                if self.min_trip_time > 0:
                    return
            # Choose an action
            p_action = random.random()
            if p_action < PROBA_PENALTY:
                self.penalty_action()  # random penalty
            elif p_action < PROBA_PENALTY + PROBA_NEW_TRIP:  # score if start cover
                self.new_trip()
                self.min_trip_time = random.randint(10, 15)
            elif p_action < PROBA_PENALTY + PROBA_NEW_TRIP + 0.1:
                self.random_speech()  # speaker says something about the pivot
