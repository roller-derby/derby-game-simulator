import random


from derbygame.team import Player, POSITION_PIVOT
from .pivot import Flamingos, Speaker, Pivot, PROBA_PENALTY, PROBA_NEW_TRIP

PROBA_STAR_PASS = 0.05
PROBA_CALL = 0.2


class Jammer(Pivot):
    def __init__(
        self,
        player: Player = Player(),
        flamingos: Flamingos = Flamingos(),
        speaker: Speaker = Speaker(),
        second_duration: float = 1.0,
    ):
        Pivot.__init__(self, player, flamingos, speaker, second_duration)

        self.has_star_cover = True  # Has the start cover
        self.lead = False  # Has not (yet) the lead
        self.pivot = None  # Pivot on the same team
        self.chosen_as_lead = False  # Is chosen for obtain the lead

    def set_pivot(self, pivot: Pivot):
        self.pivot = pivot

    def chose_as_lead(self):
        self.chosen_as_lead = True

    def call(self):
        """Call the end of the jam"""

        # Speaker
        self.speaker.speaker_4_nso("call !", self.player, ["SK"])
        self.speaker.speak_whistle_stop_jam_ref()

        # Ends the jam
        self.stop()

        # Flamingos
        self.flamingos.call(self.player.get_color())

    def star_pass(self):
        """Star pass"""

        # No star pass if pivot is in box
        if (self.pivot is None) or (self.pivot.is_in_box()):
            return False

        # Speaker
        self.speaker.speak_star_pass_j(self.player)

        # Pivot => Jammer
        self.speaker.speak_star_pass_p(self.pivot.player)
        self.pivot.star_pass()
        self.flamingos.star_pass(self.pivot.get_number(), self.player.get_color())

        # Jammer => Pivot
        self.lead = False
        self.has_star_cover = False
        self.player.set_position(POSITION_PIVOT)

    def obtain_lead(self):
        """ " Obtains the lead"""

        self.lead = True
        self.chosen_as_lead = False

        self.speaker.speak_whistle_lead()
        self.flamingos.win_lead(self.player.get_color())

    def loose_lead(self):
        """Jammer looses the lead"""

        if self.lead:
            self.flamingos.lost_lead(self.player.get_color())

        self.lead = False  # No lead
        self.chosen_as_lead = False  # No chance to obtain lead

    def penalty_action(self) -> bool:
        is_penalty = super().penalty_action()
        if is_penalty:
            self.loose_lead()
        return is_penalty

    def core_action(self) -> None:
        # Box actions
        box_action = self.box_action()
        if not box_action:
            # No action Jammer is outside the pack
            if self.has_star_cover:
                self.min_trip_time -= 1
                if self.min_trip_time > 0:
                    return

            p_action = random.random()

            # Score
            if p_action < PROBA_NEW_TRIP:  # score if start cover
                num_trip = self.new_trip()
                self.min_trip_time = random.randint(10, 15)
                if (num_trip == 1) and self.chosen_as_lead:
                    self.obtain_lead()
            # Start pass
            elif self.has_star_cover and (p_action < PROBA_NEW_TRIP + PROBA_STAR_PASS):
                self.star_pass()
            # Call
            elif self.lead and (
                p_action < PROBA_NEW_TRIP + PROBA_STAR_PASS + PROBA_CALL
            ):
                self.call()
            # Penalty
            elif (
                p_action < PROBA_PENALTY + PROBA_NEW_TRIP + PROBA_STAR_PASS + PROBA_CALL
            ):
                self.penalty_action()
            # Speaker comment
            elif (
                p_action
                < PROBA_PENALTY + PROBA_NEW_TRIP + PROBA_STAR_PASS + PROBA_CALL + 0.1
            ):
                self.random_speech()  # speaker says something about the pivot
