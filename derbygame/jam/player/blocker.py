import logging
import random

from derbygame.official import Flamingos
from derbygame.team import Player
from derbygame.speaker import Speaker

from derbygame.jam.utils.base_jam_object import BaseJamObject
from derbygame.jam.utils.penalty_generator import PenaltyGenerator

PROBA_PENALTY = 0.1


class Blocker(BaseJamObject):
    """Blocker"""

    def __init__(
        self,
        player: Player = Player(),
        flamingos: Flamingos = Flamingos(),
        speaker: Speaker = Speaker(),
        second_duration: float = 1.0,
    ):
        # Blocker do no actions during FACTOR_SLEEPING seconds
        BaseJamObject.__init__(
            self, object_name="Blocker", sleeping_time=second_duration
        )

        # Objects
        self.player: Player = player
        self.flamingos: Flamingos = flamingos
        self.speaker: Speaker = speaker

        # Penalty generator
        self.penalty_generator = PenaltyGenerator(
            language=self.speaker.get_language(),
            position=self.player.get_position(),
            proba=player.get_proba_penalty(),
        )

        # Indicate if the blocker has to go to the box
        self.go_to_box = False

    def is_in_box(self):
        return self.player.is_in_box()

    def arrive_in_box(self):
        self.speaker.speak_arrive_box(self.player)
        self.flamingos.arrive_in_box(
            player_position="B",
            player_num=self.player.get_number(),
            team_color=self.player.get_color(),
            time_to_serve=self.player.get_time_to_serve(),
        )

    def leave_box(self):
        self.speaker.speak_out_box(self.player)
        self.flamingos.leave_box(self.player.get_number(), self.player.get_color())

    def penalty_action(self) -> bool:
        """Penalty action"""

        # Ask how many blockers are in box
        num_blocker_box = self.flamingos.get_blocker_in_box(self.player.get_color())
        # No penalty
        # TODO: manage queue
        if num_blocker_box >= self.penalty_generator.get_max_blocker_box():
            return False

        # Generate penalty (0, 1 or more)
        l_penalty = self.penalty_generator.generate()
        if len(l_penalty) > 0:
            # Describes the penalty fault
            description = ""
            time_to_serve = 0
            for d_penalty in l_penalty:
                description += d_penalty["description"]
                time_to_serve += d_penalty["duration"]
            # Notify the speaker
            self.speaker.speaker_4_nso(description, self.player, ["PT"])
            self.speaker.speaker_ref("TWIIIIT")
            # Notify flamingos
            self.flamingos.new_penalty(
                player_id=self.player.get_number(),
                team_color=self.player.get_color(),
                penalty_ids=[penalty["letter"] for penalty in l_penalty],
            )
            # Add time to serve
            self.player.set_time_to_serve(time_to_serve=time_to_serve)
            # Player has to go to the box
            self.go_to_box = True
            return True

        return False

    def random_speech(self):
        """Random speach"""
        self.speaker.speaker_random(self.player)

    def box_action(self) -> bool:
        box_action = True
        if self.go_to_box:  # arrive in box
            self.arrive_in_box()
            self.go_to_box = False
        elif self.player.is_in_box():  # stay one second in box
            self.player.stay_one_second_in_box()
            if self.player.time_to_leave_box():
                self.leave_box()
        else:
            box_action = False
        return box_action

    def core_action(self) -> None:
        box_action = self.box_action()
        if not box_action:
            p_action = random.random()
            if p_action < PROBA_PENALTY:
                self.penalty_action()
            elif p_action < PROBA_PENALTY + 0.1:
                self.random_speech()  # speaker says something about the blocker

    def last_action(self):
        logging.debug("Last %s action", self.object_name)
        if self.go_to_box:  # go to box if penalty
            self.arrive_in_box()
            self.go_to_box = False
