import logging
import yaml
from typing import Dict

from derbygame.official import Timer
from derbygame.speaker import Speaker
from .utils.base_jam_object import BaseJamObject


class JamTimer(BaseJamObject):
    """Class of JamTimerSimulator"""

    def __init__(self, speaker: Speaker = Speaker(), second_duration: float = 1.0):
        BaseJamObject.__init__(
            self, object_name="JamTimer", sleeping_time=second_duration
        )

        with open("conf/rules/game.yaml") as file:
            game_conf: Dict[str, object] = yaml.load(file, Loader=yaml.FullLoader)
        self.max_jam_duration = game_conf.get("jam_max_duration", 120)
        self.internal_time = 0
        self.early_stop = True
        self.timer = Timer()
        self.speaker = speaker

    def first_action(self):
        self.internal_time = 0
        self.timer.reset()
        logging.debug("start jam")

    def core_action(self) -> None:
        # Stop the jam if it is the max jam duration
        if self.internal_time == self.max_jam_duration:
            self.early_stop = False
            self.stop()
            return

        # Add one second
        self.timer.add_second()  # Add one second to the timer
        self.internal_time += 1
        logging.debug("Jamtimer: %ss", self.internal_time)

    def last_action(self):
        if not self.early_stop:
            self.speaker.speak_whistle_stop_jam_jt()

    def get_jam_time(self):
        return self.timer.get_jam_time()

    def get_early_stop(self):
        return self.early_stop
