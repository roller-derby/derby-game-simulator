import asyncio

from derbygame.team import Player

from .utils.base_jam_object import BaseJamObject
from .factory import JamObjectFactory
from .jamtimer import JamTimer
from .player.jammer import Jammer, Pivot


class Jam(BaseJamObject):
    def __init__(self, jam_factory: JamObjectFactory = JamObjectFactory()):
        BaseJamObject.__init__(
            self,
            object_name="Jam",
            sleeping_time=jam_factory.get_second_duration() / 11,
        )

        self.jam_factory: JamObjectFactory = jam_factory
        self.jam_objects: list[BaseJamObject] = []
        self.lead: Jammer = None
        self.jam_timer: JamTimer = None

    def start(
        self,
        l_player: list[Player],
        lead_jammer_name: str,
        l_jammer_pivot: list[tuple[str, str]],
    ) -> int:
        # Construct jam objects
        d_jam_player: dict[BaseJamObject] = {}
        jam_timer: JamTimer = self.jam_factory.jam_timer()
        l_jam_objects: list[BaseJamObject] = [jam_timer]
        for player in l_player:
            jam_player = self.jam_factory.player(player=player)
            d_jam_player[player.get_derby_name()] = jam_player
            l_jam_objects.append(jam_player)

        # Update Jam objects
        # - choose lead jammer
        lead_jammer: Jammer = d_jam_player[lead_jammer_name]
        lead_jammer.chose_as_lead()
        # - add Pivot to Jammer
        for jammer_name, pivot_name in l_jammer_pivot:
            jammer: Jammer = d_jam_player[jammer_name]
            pivot: Pivot = d_jam_player[pivot_name]
            jammer.set_pivot(pivot=pivot)
        # - remove dict of player
        del d_jam_player

        # Jam objects
        self.jam_objects = l_jam_objects
        self.lead = lead_jammer  # - Record lead player
        self.jam_timer = jam_timer

        # Run Jam objects util the jam was stopped
        self.jam = True
        asyncio.run(self.class_task())

        return self.jam_timer.get_jam_time()

    def first_action(self) -> None:
        """Start all "jam" objects (JamTimer and Players)"""
        for jam_object in self.jam_objects:
            jam_object.start()

    def core_action(self) -> None:
        """Stop the jam id the Jam Timer stop or the lead jammer stop the jam"""
        # Stop Jam is the lead jammer stopped the jam or if the Jam Timer stopped the jam
        if not (self.lead.is_running() and self.jam_timer.is_running()):
            self.stop()

    def last_action(self):
        """Stop all jam objects (JamTimer and Players)"""
        for jam_object in self.jam_objects:
            jam_object.stop()
