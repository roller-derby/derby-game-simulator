import os
import shutil
import yaml

from derbygame.team import Player, POSITION_JAMMER, POSITION_PIVOT, POSITION_BLOCKER
from .nso import ScoreKeeper, PenaltyBox, LineUpTracker, PenaltyTracker, BaseNSO
from .scoreboard import ScoreBoard


class Flamingos:
    """Class of all NSO"""

    def __init__(self, d_roster: dict = {}):
        self.scoreboard = ScoreBoard()  # Scoreboard object is shared with all NSO
        self.d_other_color: dict[str, list[str]] = dict()  # Dictionary of other color
        self.d_nso: dict[str, BaseNSO] = dict()  # Dictionnary of NSO
        self.s_liberate_jammer = set()  # Set of jammer to liberate

        # Conf NSO
        with open("conf/rules/nso.yaml") as file:
            conf_nso: dict[str, object] = yaml.load(file, Loader=yaml.FullLoader)
        self.file_path = conf_nso.get("file_path", "stats")
        conf_nso["file_path"] = os.path.join(self.file_path, "tmp")

        # Create NSO
        for team_color in d_roster:
            # Other color
            self.d_other_color[team_color] = list()
            for other_c in d_roster:
                self.d_other_color[team_color].append(other_c)

            # Score Keeper
            id_nso = self.get_nso_id("SK", team_color)
            self.d_nso[id_nso] = ScoreKeeper(self.scoreboard, team_color, conf_nso)

            # Penalty Box
            id_nso = self.get_nso_id("PBT", team_color)
            self.d_nso[id_nso] = PenaltyBox(self.scoreboard, team_color, conf_nso)

            # Lineup Tracker
            id_nso = self.get_nso_id("LT", team_color)
            self.d_nso[id_nso] = LineUpTracker(self.scoreboard, team_color, conf_nso)

            # Penalty Tracker
            id_nso = self.get_nso_id("PT", team_color)
            self.d_nso[id_nso] = PenaltyTracker(self.scoreboard, team_color, conf_nso)

    @staticmethod
    def get_nso_id(position: str, team_color: str) -> tuple:
        """Return id NSO"""
        return position, team_color

    def get_num_jam(self) -> int:
        return self.scoreboard.get_jam_number()

    def get_num_period(self) -> int:
        return self.scoreboard.get_period_number()

    def new_jam(self, d_position: dict[str, list[Player]]):
        """New jam"""
        # Score board
        id_jam = self.scoreboard.new_jam()

        # All NSO
        for id_nso in self.d_nso:
            nso = self.d_nso[id_nso]
            nso.new_jam()

        # Update NSO specificity
        # -- Jammer
        d_lt: dict = {}
        for player in d_position.get(POSITION_JAMMER, []):
            color: str = player.get_color()
            player_id = player.get_number()
            id_sk = self.get_nso_id("SK", color)
            # - Score Keeper
            sk: ScoreKeeper = self.d_nso[id_sk]
            sk.set_jammer(player_id)
            # - LineUp Tracker
            d_lt[color] = {POSITION_JAMMER: player_id}
        # -- Pivot
        for player in d_position.get(POSITION_PIVOT, []):
            color: str = player.get_color()
            player_id = player.get_number()
            id_sk = self.get_nso_id("SK", color)
            # - LineUp Tracker
            d_lt[color][POSITION_PIVOT] = player_id
        # -- Blocker
        d_num_blocker: dict = {}
        for player in d_position.get(POSITION_BLOCKER, []):
            color: str = player.get_color()
            player_id = player.get_number()
            id_sk = self.get_nso_id("SK", color)
            # - LineUp Tracker
            if color not in d_num_blocker:
                d_num_blocker[color] = 1
            d_lt[color][f"{POSITION_BLOCKER}_{d_num_blocker[color]}"] = player_id
            d_num_blocker[color] += 1

        # Lineup tracker
        for team_color in d_num_blocker:
            id_lt = self.get_nso_id("LT", team_color)
            lt: LineUpTracker = self.d_nso[id_lt]
            lt.new_lineUp(d_line=d_lt[team_color])

        return id_jam

    def new_period(self):
        """New period"""

        return self.scoreboard.new_period()

    def get_archive_path(self):
        return os.path.join(self.file_path, "stats.zip")

    def end_game(self) -> str:
        """End other"""

        # All NSO print their sheet in a file
        for id_nso in self.d_nso:
            nso = self.d_nso[id_nso]
            nso.close_jam()
            nso.print_sheet()

        # Delete archive
        archive_path_2 = self.get_archive_path()
        if os.path.exists(archive_path_2):
            os.remove(archive_path_2)

        # Create archive
        archive_path = os.path.join(self.file_path, "tmp")
        shutil.make_archive("stats/stats", "zip", archive_path)

        print(f"Stats path: {self.get_archive_path()}")

    def star_pass(self, id_pivot, team_color: str):
        """Star pass"""

        # Scorekeeper
        nso_id = self.get_nso_id("SK", team_color)
        sk: ScoreKeeper = self.d_nso.get(nso_id)
        if sk is None:
            return
        sk.star_pass(id_pivot)

        # LineUp Tracker
        id_lt = self.get_nso_id("LT", team_color)
        lt: LineUpTracker = self.d_nso[id_lt]
        lt.star_pass()

        # Notify NSO from other teams
        other_colors = self.d_other_color[team_color]
        for color in other_colors:
            # -- Scorekeeper
            sk_id = self.get_nso_id("SK", color)
            sk = self.d_nso.get(sk_id)
            sk.star_pass_other()
            # -- LineUp Tracker
            id_lt = self.get_nso_id("LT", color)
            lt: LineUpTracker = self.d_nso[id_lt]
            lt.star_pass_other()

    def win_lead(self, team_color: str):
        """Win the lead"""

        # Scorekeeper
        nso_id = self.get_nso_id("SK", team_color)
        sk: ScoreKeeper = self.d_nso.get(nso_id)
        if sk is not None:
            sk.win_lead()

    def new_trip(self, team_color: str, nb_legal_pass: int = 4) -> int:
        """Score"""

        # ScoreKeeper
        nso_id = self.get_nso_id("SK", team_color)
        sk: ScoreKeeper | None = self.d_nso.get(nso_id)
        if sk is None:
            return 0
        num_trip = sk.new_trip(nb_legal_pass)

        return num_trip

    def lost_lead(self, team_color: str):
        """Lost the lead"""

        # ScoreKeeper
        nso_id = self.get_nso_id("SK", team_color)
        sk: ScoreKeeper = self.d_nso.get(nso_id)
        if sk is not None:
            sk.lost_lead()

    def call(self, team_color: str):
        """Call the end of the jam"""

        # ScoreKeeper
        nso_id = self.get_nso_id("SK", team_color)
        sk: ScoreKeeper = self.d_nso.get(nso_id)
        if sk is not None:
            sk.call()

    def new_penalty(self, player_id: str, team_color: str, penalty_ids: list[str]):
        nso_id = self.get_nso_id("PT", team_color)
        pt: PenaltyTracker = self.d_nso.get(nso_id)
        pt.write_penalties(player_id=player_id, penalties=penalty_ids)

    def arrive_in_box(
        self, player_num: str, player_position: str, team_color: str, time_to_serve: int
    ):
        """Player arrives in box"""

        # Penalty Box
        nso_id = self.get_nso_id("PBT", team_color)
        pbt: PenaltyBox = self.d_nso.get(nso_id)
        pbt.add_player(player_num, player_position, time_to_serve)

        # LineUp Tracker
        id_lt = self.get_nso_id("LT", team_color)
        lt: LineUpTracker = self.d_nso[id_lt]
        lt.arrive_in_box(player_id=player_num)

    def leave_box(self, player_num: str, team_color: str):
        # PenaltyBox
        nso_id = self.get_nso_id("PBT", team_color)
        pbt: PenaltyBox = self.d_nso.get(nso_id)
        pbt.remove_player(player_num)

        # LineUp Tracker
        id_lt = self.get_nso_id("LT", team_color)
        lt: LineUpTracker = self.d_nso[id_lt]
        lt.leave_box(player_id=player_num)

        # Remove from set of liberate jammer
        if player_num in self.s_liberate_jammer:
            self.s_liberate_jammer.remove(player_num)

    def is_pivot_in_box(self, team_color: str) -> bool:
        nso_id = self.get_nso_id("PBT", team_color)
        pbt: PenaltyBox | None = self.d_nso.get(nso_id)
        if pbt is None:
            return False
        return pbt.pivot_in_box()

    def get_blocker_in_box(self, team_color: str) -> int:
        # PenaltyBox Timer
        nso_id = self.get_nso_id("PBT", team_color)
        pbt: PenaltyBox = self.d_nso.get(nso_id)
        return pbt.get_blocker_number()

    def get_other_blocker_in_box(self, team_color: str) -> int:
        """Return the number of adv. player in box"""

        # Other team colors
        l_other_color: list[str] = self.d_other_color.get(team_color, [])

        # Count the number of player with other color in box
        nb = 0
        for color in l_other_color:
            # Find the PBT
            nso_id = self.get_nso_id("PBT", color)
            pbt: PenaltyBox = self.d_nso.get(nso_id)
            # Ask the PBT
            if pbt is not None:
                nb += pbt.get_blocker_number()
        return nb

    def check_jammer_in_box(self, team_color: str):
        """
        Check if there are other jammer in box, liberate other jammer
        """

        # Other team colors
        l_other_color: list[str] = self.d_other_color.get(team_color, [])

        # Check other jammer
        all_time = 0
        nb_other_jammer = 0
        for color in l_other_color:
            # Find the PBT
            nso_id = self.get_nso_id("PBT", color)
            pbt: PenaltyBox = self.d_nso[nso_id]
            # Ask the pbt if a jammer is in box
            jammer_other, time_other = pbt.jammer_liberate_jammer()

            # If a jammer is in box
            if jammer_other is not None:
                # Jammer leave the box
                self.s_liberate_jammer.add(jammer_other)
                # Add time in box
                all_time += time_other
                # Count the number of jammer in box
                nb_other_jammer += 1

        return all_time // max(nb_other_jammer, 1)

    def ask_4_liberate(self, jammer_id):
        """A jammer ask if an other jammer is arrived in box"""

        if jammer_id in self.s_liberate_jammer:
            self.s_liberate_jammer.remove(jammer_id)
            return True

        return False
