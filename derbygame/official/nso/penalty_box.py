import copy

from .base_nso import BaseNSO, ScoreBoard


class PenaltyBox(BaseNSO):
    """Class of Penalty Box"""

    def __init__(
        self, score_board: ScoreBoard, team_color: str, conf_nso: dict[str, object] = {}
    ):
        # PBT is an NSO
        BaseNSO.__init__(
            self,
            score_board=score_board,
            position="PBT",
            team_color=team_color,
            conf_nso=conf_nso,
        )

        # Timer
        self.pbt_timer = 0

        # Players
        # - Number of players in box
        self.nb_blocker = 0
        self.jammer_in_box: bool = False
        self.pivot_in_box: bool = False
        # - Jammer number
        self.jammer_number = None
        # - Currents lines (one for each player in box)
        self.d_current_lines = dict()

    def close_jam(self):
        """End of the jam period"""

        # Write timer seconds for all players in box
        for id_p in self.d_current_lines:
            current_line = self.d_current_lines[id_p]

            flag = True
            i = 1
            while flag:
                id_col = "SEJ_" + str(i)
                if current_line[self.map_header[id_col]] == "":
                    current_line[self.map_header[id_col]] = (
                        self.scoreboard.get_jam_total_time() - self.pbt_timer
                    )
                    flag = False
                i += 1

    def new_jam(self):
        """Start a new jam"""

        # Write in the column SEJ
        self.close_jam()

    def get_blocker_number(self):
        """Return the number of blocker in box"""

        return self.nb_blocker

    def get_jammer_in_box(self) -> bool:
        return self.jammer_in_box

    def get_pivot_in_box(self) -> bool:
        return self.pivot_in_box

    def remove_player(self, player_num: str):
        """Remove player"""

        if player_num not in self.d_current_lines:
            return False

        player_line = self.d_current_lines[player_num]
        player_position = player_line[self.map_header["POS"]]

        # Count the number of player in box
        if player_position[0] == "J":
            self.jammer_in_box = False
            self.jammer_number = None
        else:
            if player_position[0] == "P":
                self.pivot_in_box = False
            self.nb_blocker = self.nb_blocker - 1

        # Remove player on current lines
        if player_num in self.d_current_lines:
            del self.d_current_lines[player_num]

    def add_player(self, player_num: str, player_position: str, time_to_serve: int):
        """Add a player in box"""

        # PBT Timer
        if (self.nb_blocker == 0) and (not self.jammer_in_box):
            self.pbt_timer = self.scoreboard.get_jam_total_time()

        # Count the number of player in box
        if player_position[0] == "J":
            self.jammer_in_box = True
            self.jammer_number = player_num
        else:
            if player_position[0] == "P":
                self.pivot_in_box = True
            self.nb_blocker += 1

        # Add player line
        player_line = copy.deepcopy(self.empty_line)
        self.d_current_lines[player_num] = player_line
        self.sheet.append(player_line)

        # Complete line player
        player_line[self.map_header["PERIOD"]] = self.scoreboard.get_period_number()
        player_line[self.map_header["JAM"]] = self.scoreboard.get_jam_number()
        player_line[self.map_header["TEAM"]] = (self.team_color[0]).upper()
        player_line[self.map_header["POS"]] = player_position[0]
        player_line[self.map_header["#PLAYER"]] = player_num

        current_time = self.scoreboard.get_jam_total_time()
        player_line[self.map_header["IN"]] = current_time - self.pbt_timer
        player_line[self.map_header["STAND"]] = (
            current_time + time_to_serve - 10 - self.pbt_timer
        )
        player_line[self.map_header["DONE"]] = (
            current_time + time_to_serve - self.pbt_timer
        )

    def jammer_liberate_jammer(self):
        """Set the waiting time for the jammer in box"""

        if not self.jammer_in_box:
            return None, -1

        player_num = self.jammer_number
        jammer_line = self.d_current_lines[player_num]

        # Write on sheet
        current_time = self.scoreboard.get_jam_total_time() - self.pbt_timer
        jammer_line[self.map_header["STAND"]] = min(
            current_time, jammer_line[self.map_header["STAND"]]
        )
        jammer_line[self.map_header["DONE"]] = current_time

        # Time stay in box
        stay_in_box = current_time - jammer_line[self.map_header["IN"]]

        return player_num, stay_in_box
