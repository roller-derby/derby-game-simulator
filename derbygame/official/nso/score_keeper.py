import copy
from .base_nso import BaseNSO, ScoreBoard


class ScoreKeeper(BaseNSO):
    """Class of Scorekeeper"""

    def __init__(
        self, score_board: ScoreBoard, team_color: str, conf_nso: dict[str, object] = {}
    ):
        # ScoreKeeper is an NSO
        BaseNSO.__init__(
            self,
            score_board=score_board,
            position="SK",
            team_color=team_color,
            conf_nso=conf_nso,
        )

        # Init
        # - Number of trip
        self.nb_trip = 0
        # - Point
        self.total_jam = 0
        self.total_period = 0

        self.line_sp = copy.deepcopy(self.empty_line)  # Star pass *
        self.line_sp[self.map_header["#JAMMER"]] = "SP *"

    def no_initial(self) -> bool:
        """Check if no initial pass"""

        # Write no initial pass
        if self.nb_trip == 0:
            # Add a cross in columns 'IN'
            self.current_line[self.map_header["IN"]] = self.cross
            return True

        return False

    def count_point(self):
        """Count points"""

        self.current_line[self.map_header["TOTAL_JAM"]] = self.total_jam
        self.total_period += self.total_jam
        self.total_jam = 0

        self.current_line[self.map_header["TOTAL"]] = self.total_period

    def close_jam(self):
        """End of the jam"""

        # No jam yet
        if self.scoreboard.get_jam_number() == 0:
            return False

        # Fill sheet
        self.no_initial()
        self.count_point()

        # Reset number of trip
        self.nb_trip = 0

        return True

    def new_jam(self):
        """Start a new jam"""

        # Close the current jam
        self.close_jam()

        # Start a new line
        # - Fill line
        self.current_line = copy.deepcopy(self.empty_line)
        self.current_line[self.map_header["JAM"]] = self.scoreboard.get_jam_number()
        # - Add current line in the sheet
        self.sheet.append(self.current_line)

    def set_jammer(self, num_jammer):
        self.current_line[self.map_header["#JAMMER"]] = num_jammer

    def star_pass(self, id_pivot: str):
        """Star pass"""

        # Fill current line
        # - Check no initial
        self.no_initial()
        # - Count total points
        self.count_point()

        # Start a new line
        # - Start from an empty line
        self.current_line = copy.deepcopy(self.empty_line)
        # - Add Pivot information
        self.current_line[self.map_header["JAM"]] = self.scoreboard.get_jam_number()
        self.current_line[self.map_header["#JAMMER"]] = "SP" + id_pivot
        # - Add line in the sheet
        self.sheet.append(self.current_line)

    def star_pass_other(self):
        """Star pass adversary."""

        # Add SP* line in the sheet
        self.sheet.append(self.line_sp)

    def win_lead(self):
        """The jammer obtains the lead"""

        self.current_line[self.map_header["LEAD"]] = self.cross

    def lost_lead(self):
        """The jammer looses the lead"""

        self.current_line[self.map_header["LOST"]] = self.cross

    def call(self):
        """Lead jammer call the end of the jam"""

        self.current_line[self.map_header["CALL"]] = self.cross

    def new_trip(self, legal_pass: int):
        """New trip"""

        # Score after initial pass
        if self.nb_trip > 0:
            self.total_jam += legal_pass
            self.current_line[self.map_header["TRIP_2"] + self.nb_trip] = legal_pass

        # Count the number of trips
        self.nb_trip += 1

        return self.nb_trip
