from .base_nso import BaseNSO, ScoreBoard


class PenaltyTracker(BaseNSO):
    """Class of Penalty Box"""

    def __init__(
        self, score_board: ScoreBoard, team_color: str, conf_nso: dict[str, object] = {}
    ):
        # PT is an NSO
        BaseNSO.__init__(
            self,
            score_board=score_board,
            position="PT",
            team_color=team_color,
            conf_nso=conf_nso,
        )

        # Penalty players
        self.pp: dict[str, list[tuple[int, str]]] = {}

    def _add_players(self, l_payers_ids: list[str]) -> None:
        for player_id in l_payers_ids:
            self.pp[player_id] = []

    def write_penalties(self, player_id: str, penalties: list[str]) -> None:
        if player_id not in self.pp:
            self.pp[player_id] = []

        jam_id = self.scoreboard.get_jam_number()
        for p_id in penalties:
            self.pp[player_id].append((jam_id, p_id))

    def fill_sheet(self):
        # Order players by id
        player_ids = list(self.pp.keys())
        player_ids.sort()

        # Fill the sheet
        for player_id in player_ids:
            line_jam = [
                *[
                    player_id,
                ],
                *[t[0] for t in self.pp[player_id]],
            ]
            line_penalties = [
                *[
                    player_id,
                ],
                [t[1] for t in self.pp[player_id]],
            ]
            self.sheet.append(line_jam)
            self.sheet.append(line_penalties)

    def print_sheet(self) -> str:
        self.fill_sheet()
        return super().print_sheet()

    def new_jam(self):
        pass

    def close_jam(self):
        pass
