import csv
import os
from abc import ABC, abstractmethod

from derbygame.official.scoreboard import ScoreBoard


class BaseNSO(ABC):
    """Base class for NSO"""

    def __init__(
        self,
        score_board: ScoreBoard,
        position: str,
        team_color: str,
        conf_nso: dict,
    ):
        self.scoreboard = score_board
        self.position = position
        self.team_color = team_color
        self.file_path = conf_nso.get("file_path", "stats")
        self.cross = conf_nso.get("cross", "x")

        # NSO Sheet
        sheet_header = conf_nso.get(position, [])
        self.sheet = [sheet_header]  # add header line to the sheet
        self.current_line = ["" for _ in sheet_header]
        self.empty_line = ["" for _ in sheet_header]
        self.map_header = {head: i for i, head in enumerate(sheet_header)}

    def __str__(self):
        aff = ""
        for line in self.sheet:
            aff += "\n" + str(line)

        return aff

    def get_position(self):
        return self.position

    def get_team_color(self):
        return self.team_color

    def print_sheet(self) -> str:
        """Write the NSO sheet"""

        nso_folder = self.file_path
        os.makedirs(nso_folder, exist_ok=True)

        file_name = f"{self.position}_{self.team_color}.csv"
        file_path = os.path.join(nso_folder, file_name)

        with open(file_path, "w", newline="") as f:
            writer = csv.writer(f)
            for line in self.sheet:
                writer.writerow(line)

    @abstractmethod
    def new_jam(self):
        pass

    @abstractmethod
    def close_jam(self):
        pass
