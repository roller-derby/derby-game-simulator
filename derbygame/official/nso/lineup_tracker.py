import copy

from derbygame.team import POSITION_JAMMER, POSITION_PIVOT

from .base_nso import BaseNSO, ScoreBoard


class LineUpTracker(BaseNSO):
    """Class of LineUp Tracker"""

    def __init__(
        self, score_board: ScoreBoard, team_color: str, conf_nso: dict[str, object] = {}
    ):
        # LineUp Tracker is an NSO
        BaseNSO.__init__(
            self,
            score_board=score_board,
            position="LT",
            team_color=team_color,
            conf_nso=conf_nso,
        )

        # Record player in box
        self.d_box = {}

    def new_jam(self):
        # Close the current jam
        self.close_jam()

        # New line
        new_line = copy.copy(self.empty_line)
        new_line[self.map_header["Jam"]] = self.scoreboard.get_jam_number()
        self.current_line = new_line
        self.sheet.append(self.current_line)

    def close_jam(self):
        # No yet jam
        if self.scoreboard.get_jam_number() == 0:
            return

        # Manage box cells
        # - Find players outside the box
        free_player = []
        for player_id in self.d_box:
            cell_id = self.d_box[player_id]
            if self.current_line[cell_id] == "+" or self.current_line[cell_id] == "$":
                free_player.append(player_id)

        # - Remove players outside the box
        for player_id in free_player:
            del self.d_box[player_id]

    def new_lineUp(self, d_line: dict[str, str]):
        no_pivot = True

        # Add a new line
        for id_position in d_line:
            player_id = d_line[id_position]
            # - Check presence of a Pivot
            if id_position == POSITION_PIVOT:
                no_pivot = False
            # - Add player in jam line
            self.current_line[self.map_header[id_position]] = player_id
            if player_id in self.d_box:
                self.current_line[self.map_header[id_position] + 1] = "S"

        # - No pivot
        if no_pivot:
            self.current_line[self.map_header["noPivot"]] = self.cross

    def arrive_in_box(self, player_id: str) -> bool:
        # Add a new player in box
        find_player = player_id in self.d_box
        if not find_player:
            for i, player in enumerate(self.current_line):
                if player == player_id:
                    if player not in self.d_box:
                        self.d_box[player] = i  # Column player id

        # Player is not here ...
        if player_id not in self.d_box:
            return False

        # Increment colum where fill the penalty
        self.d_box[player_id] += 1

        # - Fill cell relative to this box arrival
        self.current_line[self.d_box[player_id]] = "-"

        return True

    def leave_box(self, player_id: str) -> bool:
        if player_id not in self.d_box:
            return False
        self.current_line[self.d_box[player_id]] = (
            "$" if self.d_box[player_id] == "S" else "+"
        )

    def star_pass(self):
        """Star pass update"""
        # New line
        new_line = copy.copy(self.current_line)
        self.current_line = new_line
        # - Fill SP
        self.current_line[self.map_header["Jam"]] = "SP"
        self.current_line[self.map_header["noPivot"]] = self.cross
        # - Switch Jammer - Pivot
        jammer_id = self.current_line[self.map_header[POSITION_JAMMER]]
        pivot_id = self.current_line[self.map_header[POSITION_PIVOT]]
        self.current_line[self.map_header[POSITION_JAMMER]] = pivot_id
        self.current_line[self.map_header[POSITION_PIVOT]] = jammer_id
        # - Update fill BOX
        # TODO: S has to be in first position BOX cell
        for i in range(2, len(self.current_line)):
            if self.current_line[i] == "-":
                self.current_line[i] = "S"  # 'S' to indicate star pass
            elif self.current_line[i] == "+":
                self.current_line[i] = ""
        # - Record current line in sheet
        self.sheet.append(self.current_line)

        # Update box
        # - Switch box Jammer - Pivot
        if jammer_id in self.d_box:
            self.d_box[jammer_id] += 4
        if pivot_id in self.d_box:
            self.d_box[pivot_id] -= 4

    def star_pass_other(self):
        line_sp = copy.copy(self.empty_line)
        line_sp[0] = "SP *"
        self.sheet.append(line_sp)
