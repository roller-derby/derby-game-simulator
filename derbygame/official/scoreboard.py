from derbygame.official.timer import Timer


class ScoreBoard:
    """Score board class"""

    def __init__(self):
        self.period = 0  # Period number
        self.num_jam = 0  # Jam number
        self.timer = Timer()  # All game timers

    def get_jam_time(self):
        return self.timer.get_jam_time()

    def get_total_time(self):
        return self.timer.get_total_time()

    def get_jam_total_time(self):
        return self.timer.get_jam_total_time()

    def new_jam(self):
        self.num_jam += 1
        return self.num_jam

    def get_jam_number(self):
        return self.num_jam

    def new_period(self):
        self.num_jam = 0
        self.period += 1
        return self.period

    def get_period_number(self):
        return self.period
