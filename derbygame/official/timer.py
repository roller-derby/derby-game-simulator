from .utils.singleton import SingletonMeta


class Timer(metaclass=SingletonMeta):
    """Timer class"""

    def __init__(self):
        self.reset_all()

    def reset_all(self):
        self.time_jam = 0  # Timer for the jam
        self.time_jam_total = 0  # Sum of jam time (useful for PenaltyBox)
        self.time_period = 0  # Timer for the period: (jam + interjam) x n

    def reset(self):
        """Reset jam timer"""
        self.time_jam = 0

    def add_second(self):
        """add one second to the jam"""
        self.add_seconds(1)

    def add_seconds(self, nb_sec: int):
        """add seconds to the jam"""
        self.time_jam += nb_sec
        self.time_jam_total += nb_sec
        self.time_period += nb_sec

    def five_seconds(self):
        self.time_period += 5

    def get_jam_time(self):
        return self.time_jam

    def get_jam_total_time(self):
        return self.time_jam_total

    def get_total_time(self):
        return self.time_period
