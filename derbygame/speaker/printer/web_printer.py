from .base_printer import BasePrinter

MAX_MESSAGE_PRINTED = 20


class WebPrinter(BasePrinter):
    def __init__(self):
        super().__init__()
        self.lines: list[str] = ["</br>" for _ in range(MAX_MESSAGE_PRINTED)]
        self.position = 0

    def print_event(self, message: dict) -> None:
        # Put the message in the list `lines`
        str_message = message.get("msg_line")
        reverse = message.get("reverse")
        color_txt = message.get("color_txt")

        color_option = color_txt
        if reverse:
            color_option += "_reverse"

        self.position = (self.position + 1) % len(self.lines)
        self.lines[self.position] = (
            f"""<div class="{color_option}">{str_message}</div>"""
        )

        print(str_message)

    def print_on_browser(self) -> str:
        aff = ""
        for i in range(self.position + 1, len(self.lines)):  # ! use join
            aff += self.lines[i]
        for i in range(self.position + 1):
            aff += self.lines[i]
        return aff
