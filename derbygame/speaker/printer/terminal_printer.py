from termcolor import colored

from .base_printer import BasePrinter


class TerminalPrinter(BasePrinter):
    def __init__(self):
        super().__init__()

    def print_event(self, message: dict) -> None:
        # Write in the terminal
        if message["reverse"]:
            str_aff = colored(
                message["msg_line"], message["color_txt"], attrs=["reverse"]
            )
        else:
            str_aff = colored(message["msg_line"], message["color_txt"])
        print(str_aff)
