import queue

from threading import Thread
from abc import ABC, abstractmethod


class BasePrinter(ABC, Thread):
    """Class of JamTimerSimulator"""

    def __init__(self):
        ABC.__init__(self)

        # Manage Thread
        Thread.__init__(self)
        self.queue = queue.Queue()
        self.game = False

    def get_queue(self):
        return self.queue

    @abstractmethod
    def print_event(self, message: dict) -> None:
        raise ValueError("Not implemented")

    def print_on_browser(self) -> str:
        return "?"

    def run(self):
        """Print messages from speaker"""

        self.game = True
        while self.game:
            try:
                message = self.queue.get(timeout=1)
            except queue.Empty:
                pass
            else:
                self.print_event(message)

    def stop(self):
        """End of the game"""

        self.game = False
