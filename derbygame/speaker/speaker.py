import os
import csv
import yaml
import random

from threading import RLock
from queue import Queue

from derbygame.official import Timer
from derbygame.team import Player


class EventGenerator(object):
    """Event generator"""

    def __init__(self, language: str = "FR"):
        with open("conf/other/comment.yaml") as file:
            comment_conf: dict[str, object] = yaml.load(file, Loader=yaml.FullLoader)

        self.language = language
        self.d_comment = comment_conf

    def generate(self, position: str) -> str:
        d_position = self.d_comment.get(position, {})
        l_comment = d_position.get(self.language, ["R.A.S"])

        id_c = random.randint(0, len(l_comment) - 1)

        return l_comment[id_c]


class Speaker:
    """Class of speaker"""

    def __init__(
        self,
        d_practice: dict = {},
        language: str = "FR",
        speaker_name: str = "Pipi Late",
        queue_printer: Queue = Queue(),
    ):
        self.timer = Timer()
        self.d_practice = d_practice
        self.language = language
        self.speaker_name = speaker_name

        # Event generator
        self.event_generator = EventGenerator(language)

        # Lock speaker
        self.lock = RLock()

        # Queue speaker
        self.message_queue = queue_printer

        # Record
        self.l_log = list()
        self.log_path = os.path.join("stats", "tmp")  # ! same as flamingo
        os.makedirs(self.log_path, exist_ok=True)
        self.file_path = os.path.join(self.log_path, "log.csv")
        self.log_lines = []

        # Config speaker about the game
        with open("conf/rules/game.yaml") as file:
            game_conf: dict[str, object] = yaml.load(file, Loader=yaml.FullLoader)

        self.arrive_box = game_conf.get("arrive_box")[language]
        self.out_box = game_conf.get("out_box")[language]
        self.lead = game_conf.get("lead")[language]
        self.initial = game_conf.get("initial")[language]
        self.score = game_conf.get("score")[language]
        self.star_pass_j = game_conf.get("star_pass_j")[language]
        self.star_pass_p = game_conf.get("star_pass_p")[language]

        self.penalty_whistle = game_conf.get("whistle_penalty")
        self.start_whistle = game_conf.get("whistle_start_jam")
        self.stop_whistle = game_conf.get("whistle_stop_jam")
        self.end_whistle = game_conf.get("whistle_end_jam")
        self.lead_whistle = game_conf.get("whistle_lead")
        self.lead = game_conf.get("lead")[language]

    def __str__(self):
        aff = "Speaker"
        aff += "\n Language:" + str(self.language)
        return aff

    def get_queue(self) -> Queue:
        return self.message_queue

    def get_lock(self):
        return self.lock

    def get_language(self):
        return self.language

    def speak(self, msg: str, color_txt="gray", name="GAME", reverse=False):
        """Main speak function

        Args:
            msg (str): Message to print
            color_txt (str, optional): color of the message. Defaults to 'gray'.
            name (str, optional): Person who is speaking. Defaults to 'GAME'.
            reverse (bool, optional): Underline the message. Defaults to False.
        """

        def add_char_left(s: str, max_size: int = 2, char="0"):
            ls = len(s)
            ss = (char * (max_size - ls)) + s
            return ss

        def add_char_right(s: str, max_size: int = 2, char="0"):
            ls = len(s)
            ss = s + (char * (max_size - ls))
            return ss

        time = self.timer.get_total_time()

        # Time info
        min = time // 60
        min_str = add_char_left(str(min))
        sec = time % 60
        sec_str = add_char_left(str(sec))
        time_str = f"[{min_str}:{sec_str}]"

        # Message
        name_str = add_char_right(f"{name}:", 16, " ")

        # Message line
        msg_line = f"{time_str} {name_str} {msg}"
        msg_line = add_char_right(msg_line, 100, " ")

        # Put message in queue
        message = {"msg_line": msg_line, "color_txt": color_txt, "reverse": reverse}
        self.message_queue.put(message)

        # Write in a log file
        msg_line = [time_str, name_str, msg]
        self.log_lines.append(msg_line)

    def print_log(self):
        with open(self.file_path, "w", newline="") as f:
            writer = csv.writer(f)
            for msg_line in self.log_lines:
                writer.writerow(msg_line)

    def speaker_random(self, player: Player):
        """Speaker describes other action"""

        msg_rand = self.event_generator.generate(player.get_position())
        msg = player.get_derby_name() + " " + msg_rand

        self.speak(msg, player.get_color(), self.speaker_name)

    def speaker_4_nso(self, msg: str, player: Player, l_practice: list):
        """Speaker describes an action that is useful for a NSO"""

        # Select if the message is pertinent for the NSO practice
        reverse = False
        for nso_id in l_practice:
            reverse = reverse or self.d_practice.get(nso_id, False)

        # Add id player to the message
        player_msg = f"{player.get_position()} {player.get_number()} ({player.get_derby_name()}) {msg}"

        # Speaker says the message
        self.speak(player_msg, player.get_color(), self.speaker_name, reverse)

    def speaker_sb(self, msg: str):
        """Speaker describes the scoreboard"""

        self.speak(msg, "white", "Score Board", reverse=True)

    def speaker_lineup(self, lineup: list[Player]):
        """Speaker describes LineUp"""

        if self.language == "FR":
            msg = "est sur le track !"
        else:
            msg = "arrives on the track!"

        for player in lineup:
            self.speaker_4_nso(msg=msg, player=player, l_practice=["LT"])

    def speaker_jt(self, msg: str):
        """Speaker describes the jamtimer"""

        self.speak(msg, "magenta", "Jam Timer", reverse=True)

    def speaker_ref(self, msg: str):
        """Speaker describes a referee"""

        self.speak(msg, "magenta", "Ref", reverse=True)

    def speak_arrive_box(self, player):
        msg = self.arrive_box
        self.speaker_4_nso(msg, player, ["PBT", "LT"])

    def speak_out_box(self, player):
        msg = self.out_box
        self.speaker_4_nso(msg, player, ["PBT", "LT"])

    def speak_lead(self, player):
        msg = self.lead
        self.speaker_4_nso(msg, player, ["SK"])

    def speak_initial(self, player):
        msg = self.initial
        self.speaker_4_nso(msg, player, ["SK"])

    def speak_score(self, player: Player, point: int):
        msg = self.score.format(point)
        self.speaker_4_nso(msg, player, ["SK"])

    def speak_star_pass_j(self, player):
        msg = self.star_pass_j
        self.speaker_4_nso(msg, player, ["SK", "LT"])

    def speak_star_pass_p(self, player):
        msg = self.star_pass_p
        self.speaker_4_nso(msg, player, ["SK", "LT"])

    def speak_whistle_penalty(self):
        msg = self.penalty_whistle
        self.speaker_ref(msg)

    def speak_whistle_lead(self):
        msg = self.lead_whistle
        self.speaker_ref(msg)

    def speak_whistle_start_jam(self):
        msg = self.start_whistle
        self.speaker_jt(msg)

    def speak_whistle_stop_jam_jt(self):
        msg = self.stop_whistle
        self.speaker_jt(msg)

    def speak_whistle_stop_jam_ref(self):
        msg = self.stop_whistle
        self.speaker_ref(msg)

    def speak_whistle_end_jam(self):
        msg = self.end_whistle
        self.speaker_ref(msg)

    def speak_test(self, msg):
        self.speaker_ref(msg)
