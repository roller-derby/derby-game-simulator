from .printer.base_printer import BasePrinter

PRINTER_TERMINAL = "terminal"
PRINTER_WEB = "web"


def printer_factory(printer_type: str = PRINTER_TERMINAL) -> BasePrinter:
    if printer_type == PRINTER_TERMINAL:
        from .printer.terminal_printer import TerminalPrinter

        return TerminalPrinter()
    elif printer_type == PRINTER_WEB:
        from .printer.web_printer import WebPrinter

        return WebPrinter()
    return None
