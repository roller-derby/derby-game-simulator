import yaml

from .player import Player
from .roster import Roster

POSITION_JAMMER = "Jammer"
POSITION_PIVOT = "Pivot"
POSITION_BLOCKER = "Blocker"


class LineUp:
    def __init__(self, d_roster: dict[str, object]):
        # Roster
        self.d_roster = d_roster

        # Conf line
        with open("conf/rules/player.yaml") as file:
            lineup_conf: dict[str, object] = yaml.load(file, Loader=yaml.FullLoader)
        self.line = lineup_conf.get("line", {"Jammer": 1, "Pivot": 1, "Blocker": 3})

        # Number of players in a line
        self.player_num = sum([int(self.line[pos]) for pos in self.line])

    def new_line(self, team_color: str) -> list[Player]:
        """
        Return the jam line
        """

        # Team roster
        team_roster: Roster = self.d_roster[team_color]

        # Players in box are staying for the next jam
        box_line, d_box = team_roster.player_in_box()

        # Choose players that are not in box
        l_new_player: list[Player] = team_roster.random_players(
            num=self.player_num - len(box_line), box=False
        )

        # Compose the new line
        new_line: list[Player] = box_line  # Players in box
        i = 0
        for position in self.line:
            # - Compute the number of open position
            nb_box = 0
            if position in d_box:
                nb_box = d_box[position]

            # - Choose players for a position
            nb_position = self.line[position] - nb_box
            for _ in range(nb_position):
                # -- Choose player
                player = l_new_player[i]
                i += 1
                # -- Set player position
                player.set_position(position)
                # -- Append player
                new_line.append(player)

        return new_line
