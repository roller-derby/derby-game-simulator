import csv
import random

from .player import Player


class Roster:
    """A Roster is a list of Players in a same team."""

    def __init__(self, team_path_file: str, color: str):
        # Color of the team
        self.team_color: str = color

        # List of player
        self.l_player: list[Player] = list()

        # Read team players file
        with open(team_path_file, newline="") as csv_file:
            reader = csv.reader(csv_file, delimiter=",")
            for line in reader:
                if len(line) == 2:
                    num, derby_name = line
                    self.l_player.append(Player(num, derby_name, self.team_color))

        # Sort players by id
        self.l_player.sort()

    def __str__(self) -> str:
        aff = "Roster {}".format(self.team_color)
        for player in self.l_player:
            aff += "\n" + str(player)
        return aff

    def get_team_color(self):
        return self.team_color

    def player_in_box(self) -> tuple:
        """Return the list of players in box and their positions."""

        l_box: list[Player] = list()
        d_position: dict[str, int] = dict()

        # Lock after all roster players
        for player in self.l_player:
            if player.is_in_box():
                # - add player
                l_box.append(player)

                # -add position
                pos = player.get_position()
                if pos not in d_position:
                    d_position[pos] = 1  # First player of this position
                else:
                    d_position[pos] += 1

        return l_box, d_position

    def random_players(self, num: int = 1, box: bool = False) -> list[Player]:
        """Randomly select num players.

        Args:
            num (int, optional): number of player to select. Defaults to 1.
            box (bool, optional): option to not select players in box. Defaults to False.

        Returns:
            list[Player]: list of selected players.
        """

        # List of players that can be selected
        l_selection = [player for player in self.l_player if player.is_in_box() == box]

        return random.sample(l_selection, num)
