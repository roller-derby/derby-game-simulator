import yaml
import random

from typing import Union


class Player(object):
    def __init__(
        self,
        player_id: Union[int, str] = 0,
        derby_name: str = "Force Trankill",
        color: str = "Pink",
    ):
        # Identification of the player
        self.number = f"#{player_id}"
        self.derby_name = derby_name
        self.color = color

        # Conf Player
        with open("conf/player/player.yaml") as file:
            player_conf: dict[str, object] = yaml.load(file, Loader=yaml.FullLoader)
        self.proba_penalty = random.uniform(
            player_conf.get("proba_penalty_min", 0.05),
            player_conf.get("proba_penalty_max", 0.25),
        )  # Probability to do a penalty

        # Penalty
        self.time_to_serve = 0  # Time to stay in box (in box if time_to_serve > 0)
        self.full_out = False  # Player full out

        # Position on track
        self.position = "X"

    def __str__(self) -> str:
        return f"{self.number} \t {self.derby_name}"

    def __lt__(self, other):
        return self.number.__lt__(other.number)

    def __eq__(self, other):
        return self.number.__eq__(other.number) and self.derby_name.__eq__(
            other.derby_name
        )

    def __hash__(self):
        return hash(self.color + str(self))

    def is_in_box(self):
        return self.time_to_serve > 0

    def get_proba_penalty(self) -> float:
        return self.proba_penalty

    def get_time_to_serve(self):
        return self.time_to_serve

    def set_time_to_serve(self, time_to_serve: int):
        self.time_to_serve = time_to_serve

    def stay_one_second_in_box(self):
        self.time_to_serve -= 1

    def time_to_leave_box(self) -> bool:
        return self.time_to_serve == 0

    def is_full_out(self):
        return self.full_out

    def assign_full_out(self):
        self.full_out = True

    def get_position(self):
        return self.position

    def set_position(self, pos: str):
        self.position = pos

    def get_color(self):
        return self.color

    def get_derby_name(self) -> str:
        return self.derby_name

    def get_number(self):
        return self.number
