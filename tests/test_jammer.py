import unittest
import asyncio

from derbygame.jam import Jammer


class TestJammer(unittest.TestCase):
    def test_jammer(self):
        jammer: Jammer = Jammer(second_duration=0.01)
        jammer.chose_as_lead()

        self.assertFalse(jammer.go_to_box)
        self.assertTrue(jammer.has_star_cover)

        # Run jam object
        async def my_test():
            jammer.start()
            await asyncio.sleep(1)
            jammer.star_pass()
            await asyncio.sleep(2)
            jammer.stop()
            await asyncio.sleep(0.1)

        asyncio.run(my_test())

        self.assertFalse(jammer.go_to_box)
        # self.assertFalse(jammer.has_star_cover)
