import unittest

from derbygame.official.timer import Timer


class TestSpeaker(unittest.TestCase):
    def test_singleton(self):
        timer_1 = Timer()
        timer_2 = Timer()
        self.assertEqual(id(timer_1), id(timer_2))
