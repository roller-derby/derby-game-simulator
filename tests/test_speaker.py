import unittest

from derbygame.speaker.speaker import Speaker


class TestSpeaker(unittest.TestCase):
    def test_init(self):
        sp_1 = Speaker()
        queue_printer = sp_1.get_queue()
        self.assertEqual(queue_printer.qsize(), 0)
