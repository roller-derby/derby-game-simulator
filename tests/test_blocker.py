import unittest
import asyncio

from derbygame.jam import Blocker


class TestBlocker(unittest.TestCase):
    def test_blocker(self):
        blocker = Blocker(second_duration=0.01)
        self.assertFalse(blocker.go_to_box)

        # Run jam object
        async def my_test():
            blocker.start()
            await asyncio.sleep(1)
            blocker.stop()
            await asyncio.sleep(0.1)

        asyncio.run(my_test())

        self.assertFalse(blocker.go_to_box)
