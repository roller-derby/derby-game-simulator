import unittest
import asyncio

from derbygame.jam.player.pivot import Pivot


class TestPivot(unittest.TestCase):
    def test_pivot(self):
        pivot: Pivot = Pivot(second_duration=0.01)
        self.assertFalse(pivot.go_to_box)

        # Run jam object
        async def my_test():
            pivot.start()
            await asyncio.sleep(1)
            pivot.star_pass()
            await asyncio.sleep(2)
            pivot.stop()
            await asyncio.sleep(0.1)

        asyncio.run(my_test())

        self.assertFalse(pivot.go_to_box)
