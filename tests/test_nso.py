import unittest
import yaml
import os

from derbygame.official.scoreboard import ScoreBoard
from derbygame.official.nso.lineup_tracker import (
    LineUpTracker,
    POSITION_JAMMER,
    POSITION_PIVOT,
)
from derbygame.official.nso.score_keeper import ScoreKeeper
from derbygame.official.nso.penalty_box import PenaltyBox
from derbygame.team.line_up import POSITION_BLOCKER


class TestNSO(unittest.TestCase):
    def setUp(self) -> None:
        self.score_board = ScoreBoard()
        with open("conf/rules/nso.yaml") as file:
            self.conf_nso: dict[str, object] = yaml.load(file, Loader=yaml.FullLoader)
        self.conf_nso["file_path"] = os.path.join("tests", "tmp")

    def test_sk(self):
        # Test constructor
        sk = ScoreKeeper(
            score_board=self.score_board, team_color="blue", conf_nso=self.conf_nso
        )
        self.assertTrue(isinstance(sk, ScoreKeeper))

        # Test new jam
        # - Jam 1
        self.score_board.new_jam()
        sk.new_jam()
        self.assertEqual(sk.current_line[0], self.score_board.get_jam_number())

    def test_pbt(self):
        # Test constructor
        pbt = PenaltyBox(
            score_board=self.score_board, team_color="blue", conf_nso=self.conf_nso
        )
        self.assertTrue(isinstance(pbt, PenaltyBox))

    def test_lt(self):
        # Test constructor
        lt = LineUpTracker(
            score_board=self.score_board, team_color="blue", conf_nso=self.conf_nso
        )
        self.assertTrue(isinstance(lt, LineUpTracker))

        # Test new jam
        # - Jam 1
        self.score_board.new_jam()
        lt.new_jam()
        self.assertEqual(lt.current_line[0], self.score_board.get_jam_number())
        # - Jam 2
        self.score_board.new_jam()
        lt.new_jam()
        self.assertEqual(lt.current_line[0], self.score_board.get_jam_number())
        self.assertEqual(len(lt.sheet), 3)

        # Test new LineUp
        d_line_up = {
            POSITION_JAMMER: "13",
            POSITION_PIVOT: "42",
            f"{POSITION_BLOCKER}_1": "01",
            f"{POSITION_BLOCKER}_2": "02",
            f"{POSITION_BLOCKER}_3": "03",
        }
        lt.new_lineUp(d_line=d_line_up)
        self.assertEqual(lt.current_line[1], "")
        self.assertEqual(lt.current_line[2], "13")

        # Test box arrival
        lt.arrive_in_box(player_id="13")
        self.assertEqual(lt.current_line[3], "-")
        self.assertIn("13", lt.d_box)
        done = lt.arrive_in_box(player_id="143")
        self.assertFalse(done)
        done = lt.arrive_in_box(player_id="03")
        self.assertTrue(done)

        # Test leave box
        lt.leave_box(player_id="13")
        self.assertEqual(lt.current_line[3], "+")

        # Star pass
        line_before = lt.current_line
        lt.star_pass()
        line_after = lt.current_line
        self.assertEqual(lt.current_line[0], "SP")
        self.assertEqual(lt.current_line[1], "x")
        self.assertEqual(line_before[2], line_after[6])
