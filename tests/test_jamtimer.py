import unittest
import asyncio

from derbygame.jam import JamTimer


class TestJamTimer(unittest.TestCase):
    def test_jamtimer(self):
        jt = JamTimer()

        # Run jam object
        async def my_test():
            jt.start()
            await asyncio.sleep(2)
            jt.stop()

        asyncio.run(my_test())

        self.assertTrue(jt.early_stop)
