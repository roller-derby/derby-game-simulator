import time
import logging
import os

import fire


class Simulator:
    def terminal(
        self,
        sk: bool = False,
        pbt: bool = False,
        lt: bool = False,
        language: str | None = None,
        time_period: int | None = None,
        second_duration: int | None = None,
    ):
        """Run game in a terminal

        Args:
            sk (bool | None, optional): fill to practice SK. Defaults to None.
            pbt (bool | None, optional): fill to practice PBT. Defaults to None.
            lt (bool | None, optional): fill to practice LT. Defaults to None.
            language (str | None, optional): language (FR or EN). Defaults to None.
            time_period (int | None, optional): number of minutes for a period. Defaults to None.
        """
        from derbygame.game import Game

        # Read app config
        app_conf = {
            "SK": sk,
            "PBT": pbt,
            "LT": lt,
            "language": language,
            "time_period": time_period,
            "second_duration": second_duration,
        }
        app_conf = {k: v for k, v in app_conf.items() if v is not None}

        # Run Game in a terminal
        game: Game = Game(app_conf=app_conf)
        game.start()
        try:
            while game.game_is_on():
                time.sleep(1)
        except KeyboardInterrupt:
            logging.warning("Early stop!")
            game.early_stop()
        else:
            game.end_game()

    def web(
        self,
        sk: bool = False,
        pbt: bool = False,
        lt: bool = False,
        language: str | None = None,
        time_period: int | None = None,
        second_duration: int | None = None,
    ) -> None:
        from front import app

        # Read app config
        app_conf = {
            "SK": sk,
            "PBT": pbt,
            "LT": lt,
            "language": language,
            "time_period": time_period,
            "second_duration": second_duration,
        }
        app_conf = {k: v for k, v in app_conf.items() if v is not None}

        # Run web app
        port = int(os.environ.get("PORT", 5000))
        app.run(host="0.0.0.0", port=port)
        # app.run(debug=True, host='0.0.0.0')


if __name__ == "__main__":
    fire.Fire(Simulator)
