import logging
import os
from flask import Flask, render_template

import shutil

from derbygame.game import Game

game = Game({"app_mode": "web"})

app = Flask(__name__)
app.debug = True


@app.route("/", methods=["GET", "POST", "DELETE"])
@app.route("/index/", methods=["GET", "POST", "DELETE"])
def index():
    return render_template(
        "index.html",
        info={
            "run": game.game_is_on(),
            "alive": game.game_is_alive(),
        },
        data=game.print_on_browser(),
        game_id=Game.get_game_id(),
    )


@app.route("/start/", methods=["GET", "POST", "DELETE"])
def start():
    global game

    # Start the game
    if not game.game_is_on() and not game.game_is_started():
        game.start()

    return render_template(
        "index.html",
        info={"run": True, "alive": game.game_is_alive()},
        data=game.print_on_browser(),
        game_id=Game.get_game_id(),
    )


@app.route("/stop/", methods=["GET", "POST", "DELETE"])
def stop():
    global game

    if game.game_is_alive():  # Early stop
        game.early_stop()
    else:  # Game is already stopped (end of the period)
        game.end_game()

    # New game
    game = Game({"app_mode": "web"})

    # Copy stats file
    os.makedirs("front/static/tmp/", exist_ok=True)
    stat_archive = "front/static/tmp/stats.zip"
    if os.path.exists(stat_archive):
        os.remove(stat_archive)
    logging.info(f"Copy stat log: '{stat_archive}'")
    shutil.copy("stats/stats.zip", "front/static/tmp/stats.zip")

    return render_template(
        "index.html",
        info={"run": False, "alive": False},
        data=game.print_on_browser(),
        game_id=Game.get_game_id(),
    )
