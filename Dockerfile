####################################################################
# Base image
####################################################################
FROM python:3.12 as base

# set the working directory in the container
WORKDIR /opt/workspace
RUN mkdir stats

# install dependencies
COPY ./pyproject.toml ./pyproject.toml
RUN python -m pip install --upgrade pip
RUN python -m pip install poetry
RUN poetry config virtualenvs.create false

####################################################################
# Dev image
####################################################################
FROM base as dev

RUN poetry add termcolor
RUN poetry add Flask
RUN poetry install

####################################################################
# Terminal image
####################################################################
FROM base as terminal

# Add termcolor package
RUN poetry add termcolor
RUN poetry install

# Copy the content of the local directories to the working directory
COPY conf conf
COPY derbygame derbygame
COPY app.py app.py

CMD ["bash"]

####################################################################
# Web image
####################################################################
FROM base as web

# Add Flask package
RUN poetry add Flask
RUN poetry install

# Copy the content of the local directories to the working directory
COPY conf conf
COPY derbygame derbygame
COPY front front
COPY app.py app.py

CMD ["python3", "app.py", "--web"]
