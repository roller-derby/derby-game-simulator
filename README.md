# Derby Game Simulator

**Derby game simulator** is a roller derby game simulator that helps practice NSO skills: 
* [Penalty Box Tracker](https://www.youtube.com/watch?v=tO_i8jU0uV0&list=PLWi76Bd9Lfode53ICVZbbrYAA2dGmHRST&ab_channel=DublinRollerDerby) (PBT)
* [Score Keeper](https://www.youtube.com/watch?v=GTBKzh24U2s&list=PLWi76Bd9Lfoc7L7wi2naXQR7uyrsTxITt&ab_channel=DublinRollerDerby) (SK)
* [LineUp Tracker](https://www.youtube.com/watch?v=107ASZUIUrc&list=PLWi76Bd9LfocXBOPcFtIS1eQH6V2XtHNq) (LT)

This project was deployed on heroku: [http://roller-derby-simulator.herokuapp.com](http://roller-derby-simulator.herokuapp.com) (fix WIP)

![](derbygame.gif)

## Run simulator using docker

### Run simulator in a web page
```
docker compose build web
mkdir stats; docker run -it -v $(pwd)/stats:/opt/workspace/stats rollerderby/simulator:web_0.2.0
```

Go to: [http://172.17.0.2:5000/](http://172.17.0.2:5000/)


### Run simulator in a terminal (advanced)
```
docker compose build terminal
mkdir stats; docker run -it -v $(pwd)/stats:/opt/workspace/stats rollerderby/simulator:terminal_0.2.0
```

Choose simulation options:

```
Options:
  --en                                   language EN(default FR)
  --sk                                   underline relevant information for *ScoreKeeper*
  --pbt                                  underline relevant information for *Penalty Box*
  --lt                                   underline relevant information for *LineUp Tracker*
  --time_period TIME_SIMU                change the period duration (TIME_SIMU in minute)
```

Example of use:

To simulate a period of 5 minutes and practice *ScoreKeeper*:

```
>> python3 app.py terminal --sk --time_period 5

```
## References

### How to fill NSO Sheet
* Score Keeper: [Tuto SK](https://www.youtube.com/watch?v=tO_i8jU0uV0&list=PLWi76Bd9Lfode53ICVZbbrYAA2dGmHRST&ab_channel=DublinRollerDerby)
* Penalty Box Tracker: [Tuto PBT](https://www.youtube.com/watch?v=GTBKzh24U2s&list=PLWi76Bd9Lfoc7L7wi2naXQR7uyrsTxITt&ab_channel=DublinRollerDerby)
* LineUp Tracker: [Tuto LT](https://www.youtube.com/watch?v=107ASZUIUrc&list=PLWi76Bd9LfocXBOPcFtIS1eQH6V2XtHNq)
* Penalty Tracker: [Tuto PT](https://www.youtube.com/watch?v=mUeY86xabfs&list=PLWi76Bd9LfofdlFbwD46ry389sy-pjMn4)